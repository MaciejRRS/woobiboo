
<?php get_header(); ?>
<main id="content">
	<article id="page-not-found">
		<div class="container">
			<div class="sign">
				<h1>404</h1>
				<p>Wygląda na to, że nie ma strony której szukałeś, ale zapraszamy na <a href="/index.php">główną</a> z pewnością zainteresuje Cię też coś innego.<p>
			</div>
		</div>
	</article>
</main>
<?php get_footer(); ?>