<?php
/**
 * Review Comments Template
 *
 * Closing li is left out on purpose!.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/review.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

use function WooBiBoo\Helpers\inline_svg;
global $product;
global $comment;
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">

    <div id="comment-<?php comment_ID(); ?>" class="comment_container">

        <?php
		/**
		 * The woocommerce_review_before hook
		 *
		 * @hooked woocommerce_review_display_gravatar - 10
		 */
		do_action( 'woocommerce_review_before', $comment );
		?>

        <div class="comment-text">

            <?php
			/**
			 * The woocommerce_review_before_comment_meta hook.
			 *
			 * @hooked woocommerce_review_display_rating - 10
			 */
			do_action( 'woocommerce_review_before_comment_meta', $comment );

			/**
			 * The woocommerce_review_meta hook.
			 *
			 * @hooked woocommerce_review_display_meta - 10
			 */
			do_action( 'woocommerce_review_meta', $comment );

			do_action( 'woocommerce_review_before_comment_text', $comment );

?>


            <div class="woocommerce-Reviews-list">
                <section class="wrap-rating-area">
                    <div class="product-rating">

                        <div class="wrapper">
                            <?php $rating = intval( get_comment_meta( $comment->comment_ID, 'rating', true ) ); ?>

                            <ul class="rating-stars rating-<?php echo $rating; ?>">
                                <li>
                                    <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/star.svg' ); ?>
                                </li>
                                <li>
                                    <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/star.svg' ); ?>
                                </li>
                                <li>
                                    <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/star.svg' ); ?>
                                </li>
                                <li>
                                    <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/star.svg' ); ?>
                                </li>
                                <li>
                                    <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/star.svg' ); ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </section>
            </div>


            <?php

			/**
			 * The woocommerce_review_comment_text hook
			 *
			 * @hooked woocommerce_review_display_comment_text - 10
			 */
			do_action( 'woocommerce_review_comment_text', $comment );

			do_action( 'woocommerce_review_after_comment_text', $comment );
?>



        </div>
    </div>