<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 5.2.0
 */

defined( 'ABSPATH' ) || exit;
?>
<div class="shop_table woocommerce-checkout-review-order-table">

		<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>

			<?php do_action( 'woocommerce_review_order_before_shipping' ); ?>

			<?php wc_cart_totals_shipping_html(); ?>

			<?php do_action( 'woocommerce_review_order_after_shipping' ); ?>

		<?php endif; ?>

	<?php do_action( 'woocommerce_review_order_before_order_total' ); ?>

    <div class="order-summary">
        <h3>
			<?php esc_html_e( 'To pay', 'woobiboo' ); ?>
        </h3>
        <dl>
            <dt><?php esc_html_e( 'Subtotal', 'woocommerce' ); ?></dt>
            <dd><?php wc_cart_totals_subtotal_html(); ?></dd>

	        <?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
                <dt><?php wc_cart_totals_coupon_label( $coupon ); ?></dt>
                <dd><?php wc_cart_totals_coupon_html( $coupon ); ?></dd>
	        <?php endforeach; ?>

            <dt><?php esc_html_e( 'Shipping', 'woocommerce' ); ?></dt>
            <dd>
                <?php echo wc_price( WC()->cart->shipping_total ); ?>
            </dd>
            <dt><?php esc_html_e( 'Total', 'woocommerce' ); ?></dt>
            <dd><?php wc_cart_totals_order_total_html(); ?></dd>
        </dl>
    </div>

	<?php do_action( 'woocommerce_review_order_after_order_total' ); ?>

</div>
