function showCouponForm() {
    document.getElementById('coupon-form').style.display = "block";
}

jQuery(document).ready(function () {


    // Mobile menu
    jQuery('#site-menu-mobile').on("click", function () {
        jQuery(this).toggleClass('active');
        jQuery(this).addClass('not-active').addClass();
        jQuery('#main-navigation').toggleClass('shown-menu');
    });

    // Main search
    jQuery('#search-trigger').on("click", function () {
        jQuery(this).toggleClass('is-open');
        jQuery('#main-navigation').toggleClass('shown-search');
    });

    // Dane do faktury
    jQuery('#billing_faktura').on("click", function () {
        jQuery('#billing_nip_field, #billing_company_field').toggle(this.checked);
    });

    jQuery(".fpf-checkbox:first").addClass("doubletablemod");

    // Added to cart notification
    jQuery(document.body).on('added_to_cart', function () {
        miniCartPopUp(true);
    });

    // Cart quantity
    var timeout;

    jQuery('form.woocommerce-cart-form input.qty').on('change', function () {
        if (timeout !== undefined) {
            clearTimeout(timeout);
        }

        timeout = setTimeout(function () {
            updateCart()
        }, 700);
    });

    // Limit to 3 colors
    jQuery('.checkboxmod :input').click(function (event) {
        var count = 0;
        jQuery('.checkboxmod :input').each(function () {
            if (this.checked) {
                count++;
            }
        }
        );
        if (count > 3) {
            event.preventDefault();
            alert('Możesz wybrac tylko 3 kolory elementów');
        }
    }
    );

});


jQuery(document).ready(function () {

    jQuery('.product_tag-switch-gallery-mod .hidden-opt, .product_tag-switch-gallery-mod-pl .hidden-opt, .product_tag-switch-gallery-mod-en .hidden-opt').hide();
    jQuery('.product_tag-switch-gallery-mod .fpf-checkbox, .product_tag-switch-gallery-mod-pl .fpf-checkbox, .product_tag-switch-gallery-mod-en .fpf-checkbox').hide();
    jQuery('#kolor').change(function () {
        if (jQuery('#kolor').val() !== 'Własne kolory') {
            jQuery('.product_tag-switch-gallery-mod .hidden-opt, .product_tag-switch-gallery-mod-pl .hidden-opt, .product_tag-switch-gallery-mod-en .hidden-opt').hide();
            jQuery('.product_tag-switch-gallery-mod .fpf-checkbox, .product_tag-switch-gallery-mod-pl .fpf-checkbox, .product_tag-switch-gallery-mod-en .fpf-checkbox').hide();
            jQuery('.product_tag-switch-gallery-mod .hidden-opt input, .product_tag-switch-gallery-mod-pl .hidden-opt input, .product_tag-switch-gallery-mod-en .hidden-opt input').prop('checked', false);
        }
    });

    jQuery('#kolor').change(function () {
        if (jQuery('#kolor').val() == 'Własne kolory') {
            jQuery('.product_tag-switch-gallery-mod .hidden-opt, .product_tag-switch-gallery-mod-pl .hidden-opt, .product_tag-switch-gallery-mod-en .hidden-opt').show();
            jQuery('.product_tag-switch-gallery-mod .fpf-checkbox, .product_tag-switch-gallery-mod-pl .fpf-checkbox, .product_tag-switch-gallery-mod-en .fpf-checkbox').show();
            jQuery("select").prop('required', true);

            jQuery('.single_add_to_cart_button').click(function () {
                if (jQuery('#kolor').val() == 'Własne kolory' && (!$(".kolorpodstawy-circle input:checked").val())) {
                    alert('Zaznacz jeden kolor podstawy!');
                    return false;
                }

            });

            jQuery('.single_add_to_cart_button').click(function () {
                if (jQuery('#kolor').val() == 'Własne kolory' && (!$(".reqvalid input:checked").val())) {
                    alert('Zaznacz przynajmniej jeden kolor elementów!');
                    return false;
                }

            });
        }
    });

});



/**
 * Auto update cart on change
 *
 * @category Cart
 * @since 3.0.0
 */
function updateCart() {

    let cartForm = document.querySelector('form.woocommerce-cart-form');

    if (cartForm) {

        let cartUpdateBtn = cartForm.querySelector('[name=update_cart]');

        cartUpdateBtn.disabled = false;
        cartUpdateBtn.click();

    }
}



/**
 * Show orders popup
 *
 * @category Mini Cart
 * @since 3.5.0
 */
function miniCartPopUp(show = true) {
    let miniCartContainer = document.getElementById('add_to_cart_popup');

    if (miniCartContainer && show) {
        miniCartContainer.style.removeProperty('display');
        setTimeout(function () {
            miniCartContainer.classList.add('is-visible');
        }, 5);
    }
}



/**
 * Hide orders popup
 *
 * @category Mini Cart
 * @since 3.5.0
 */
function closeAddToCartPopUp(button) {
    let miniCartContainer = document.getElementById('add_to_cart_popup');
    miniCartContainer.classList.remove('is-visible');
    setTimeout(function () {
        miniCartContainer.style.display = 'none';
    }, 250);
}



jQuery(window).on('load scroll resize orientationchange', function () {
    if (jQuery(this).scrollTop() > 50) {
        jQuery('.fixed-top-promo').addClass('visible');
    } else {
        jQuery('.fixed-top-promo').removeClass('visible');
    }
});



var swiper = new Swiper(".mySwiperWoo", {
    slidesPerView: "auto",
    centeredSlides: true,
    loop: true,
    slideToClickedSlide: true,
    breakpoints: {
        300: {
            spaceBetween: 10
        },
        // when window width is >= 767px
        767: {
            spaceBetween: 30
        }
    }
});


// add modal reviews
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementById('close');

// When the user clicks the button, open the modal 
btn.onclick = function () {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}




