<?php

include __DIR__ . '/vendor/autoload.php';

function wpdocs_theme_name_scripts() {
	// add library swiper for template landingPage.php
		wp_enqueue_style( 'style-swiper', get_template_directory_uri() . '/assets/css/swiper-bundle.min.css' );
		wp_enqueue_script( 'js-swiper', get_template_directory_uri() . '/assets/js/swiper-bundle.min.js', array(), null, true );
	}
	add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );
	// end library swiper

use WooBiBoo\Class_Cleanup;
use WooBiBoo\Theme_Helpers;
use WooBiBoo\Class_Setup;
use WooBiBoo\Class_Header;
use WooBiBoo\Class_Footer;
use WooBiBoo\Class_Template;
use WooBiBoo\WooCommerce\Class_WC_Global;
use WooBiBoo\WooCommerce\Class_WC_Archive;
use WooBiBoo\WooCommerce\Class_WC_Product;
use WooBiBoo\WooCommerce\Class_WC_Cart;
use WooBiBoo\WooCommerce\Class_WC_Checkout;
use WooBiBoo\WooCommerce\Class_WC_User_Account;

/**
 * WordPress cleanup
 */
$cleanup = new Class_Cleanup();
$cleanup->security();
$cleanup->clear_style();

/**
 * Theme helpers
 */
Theme_Helpers::svg_support();
Theme_Helpers::body_class();

/**
 * Theme setup
 */
new Class_Setup();

new Class_Header();
new Class_Footer();
new Class_Template();

/**
 * WooCommerce
 */
new Class_WC_Global();
new Class_WC_Archive();
new Class_WC_Product();
new Class_WC_Cart();
new Class_WC_Checkout();
new Class_WC_User_Account();






// Register thumbnails
add_theme_support( 'post-thumbnails' );
add_image_size( 'homepage-thumb', 385, 302 ); // Soft Crop Mode

//add option page to panel (ACF)
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();

}


// Update cart if click quantity or put number to input
add_action( 'wp_footer', 'cart_update_qty_script' );
function cart_update_qty_script() {
    if (is_cart()) :
    ?>
<script>
jQuery('div.woocommerce').on('change', '.quantity .qty', function() {
    //console.log('clicked');
    jQuery("[name='update_cart']").trigger("click");
});
</script>
<?php
    endif;
}






  // Remove the product rating display on product loops
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

// debug for worpdress
ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL );
define('WP_DEBUG', false);
define('WP_DEBUG_DISPLAY', false);



add_filter( 'woocommerce_available_payment_gateways', 'filter_woocommerce_available_payment_gateways' );
function filter_woocommerce_available_payment_gateways( $gateways ) {
    $session = WC()->session;

    if ( $session ) {
        $chosen_shipping_rates = $session->get( 'chosen_shipping_methods', [] );

        if ( in_array( 'flexible_shipping_6_3', $chosen_shipping_rates ) ) :
		unset( $gateways['cod'] );
		
		elseif ( in_array( 'flexible_shipping_5_2', $chosen_shipping_rates ) ) :
		unset( $gateways['bacs'] );
		unset( $gateways['payu'] );
		
	endif;
    }

    return $gateways;
}