<?php
namespace WooBiBoo;

use function WooBiBoo\Helpers\inline_svg;

/**
 * Theme Header
 * Includes all methods required by theme header
 *
 * @since   3.0
 * @author  Mateusz Major
 * @link    https://inspirelabs.pl/
 * @package woobiboo
 */
class Class_Header {

	public function __construct() {

	    // Common
		add_action( 'fh-site-navigation', array( $this, 'mobile_menu_trigger' ), 15 );
		add_action( 'fh-site-navigation', array( $this, 'main_logo' ), 20 );

		// All except cart & checkout
        add_action( 'fh-site-navigation-before', array( $this, 'advantages_bar' ), 10 );
        add_action( 'fh-site-navigation', array( $this, 'site_navigation_container_open' ), 10 );
        add_action( 'fh-site-navigation', array( $this, 'site_navigation_container_close' ), 999 );
        add_action( 'fh-site-navigation', array( $this, 'main_navigation_search' ), 30 );
        add_action( 'fh-site-navigation', array( $this, 'quick_menu' ), 40 );
        add_action( 'fh-site-navigation-after', array( $this, 'bottom_decoration' ), 999 );

        // Only checkout, cart, thank you
		add_action( 'fh-site-navigation', array( $this, 'shopping_steps' ), 40 );
		add_action( 'fh-site-navigation', array( $this, 'safe_shopping_badge' ), 60 );
		add_filter( 'woocommerce_add_to_cart_fragments', array( $this, 'wc_refresh_mini_cart_count' ) );

		// Facebook domain verification
		add_action('wp_head', array( $this, 'fb_domain_verification' ) );

	}


	public function mobile_menu_trigger() {
		if ( ! is_cart() && ! is_checkout() && ! is_order_received_page() ) :
	        ?>
                <button id="site-menu-mobile" type="button">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            <?php
        endif;
    }



	/**
	 * Top bar with advantages
	 */
	public function advantages_bar() {
		$header = get_field( 'header', 'option' );

		if ( ( $header && $header['why-us'] && array_filter( $header['why-us'] ) ) && ( ! is_cart() && ! is_checkout() && ! is_order_received_page() ) ) :
			?>
				<section class="site-navigation__advantages">
					<div class="container advantages">
						<?php
							foreach( $header['why-us'] as $advantage ) :
								if ( $advantage['description'] ) :
									?>
										<span class="advantages__item">
                                            <?php inline_svg( $advantage['icon']['url'] ); ?>
                                            <?php
                                            if ( function_exists('pll_current_language') && 'pl' != pll_current_language() && array_key_exists('description-' . pll_current_language(), $advantage ) ) :
	                                            echo esc_html( $advantage['description-' . pll_current_language() ] );
                                            else :
	                                            echo esc_html( $advantage['description'] );
                                            endif;
                                            ?>
										</span>
                                        <i class="icon icon-circle"></i>
									<?php
								endif;
							endforeach;
						?>
					</div>
				</section>
			<?php
		endif;
	}



	/**
	 * Open container for the menu
	 */
	public function site_navigation_container_open() {
		if ( ! is_cart() && ! is_checkout() && ! is_order_received_page() ) :
            ?>
                <div class="container">
            <?php
        endif;
	}



	/**
	 * Close container for the menu
	 */
	public function site_navigation_container_close() {
		if ( ! is_cart() && ! is_checkout() && ! is_order_received_page() ) :
            ?>
                </div>
            <?php
        endif;
	}



	/**
	 * Main logo
	 */
	public function main_logo() {
		if ( function_exists( 'the_custom_logo' ) ) :
			the_custom_logo();
		endif;
	}



	/**
	 * Main menu and search form
	 */
	public function main_navigation_search() {
		if ( ! is_cart() && ! is_checkout() && ! is_order_received_page() ) :
	        ?>
                <div id="main-navigation" class="container__main">
                    <?php
                        wp_nav_menu( array(
                                'theme_location' => 'primary',
                                'container' => 'nav',
                                'container_class' => 'primary__menu'
                            )
                        );

                        if ( shortcode_exists( 'fibosearch' ) ) :
                            echo do_shortcode('[fibosearch]');
                        endif;
                    ?>
                </div>
            <?php
        endif;
	}



	/**
	 * Menu with icons
	 */
	public function quick_menu() {
		global $woocommerce;

        if ( ! is_cart() && ! is_checkout() && ! is_order_received_page() ) :
        ?>
            <nav class="quick__menu">
                <button id="search-trigger" class="item">
                    <span class="search">
                        <?php inline_svg(get_template_directory_uri() . '/assets/img/search.svg' ); ?>
                    </span>
                    <span class="close">
                        <?php inline_svg(get_template_directory_uri() . '/assets/img/close.svg' ); ?>
                    </span>
                </button>
                <a class="item" href="<?php echo esc_url( get_permalink( get_option('woocommerce_myaccount_page_id') ) ); ?>">
	                <?php inline_svg(get_template_directory_uri() . '/assets/img/user.svg' ); ?>
                </a>
                <a id="mini_cart" class="item" href="<?php echo wc_get_cart_url() ?>" >
	                <?php inline_svg(get_template_directory_uri() . '/assets/img/shopping-bag.svg' ); ?>
                    <?php $this->mini_cart_counter(); ?>
                </a>
            </nav>
            <?php
        endif;
	}



	/**
	 * Cart counter
	 */
	public function mini_cart_counter() {
		$count = intval( WC()->cart->get_cart_contents_count() );
		$class = 'counter';
		if ( $count > 0 ) :
			$class = $class . ' is-visible';
		endif;
		?>

        <span id="count-products-cart" class="<?php echo esc_attr( $class ); ?>">
			<?php echo esc_html( $count ); ?>
		</span>
		<?php
	}



	/**
	 * Cart counter update
	 *
	 * @category Mini Cart
	 * @param array $fragments .
	 * @return array $fragments .
	 */
	public function wc_refresh_mini_cart_count( $fragments ) {
		ob_start();
		$this->mini_cart_counter();
		$fragments['#count-products-cart'] = ob_get_clean();
		return $fragments;
	}



	/**
	 * Colorful border decoration under main menu
	 */
	public function bottom_decoration() {

	    if ( ! is_cart() && ! is_checkout() && ! is_order_received_page() ) :
            ?>
                <div class="woobiboo-border-decoration">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            <?php
        endif;
	}



	/**
	 * Checkout steps
	 */
	public function shopping_steps() {
		if ( is_cart() || is_checkout() || is_order_received_page() ) :
            ?>
                <ul class="shopping-steps">
                    <li <?php echo ( is_cart() ? 'class="active"' : '' ) ?>>
                        <span>
                            <?php esc_html_e( 'Cart', 'woobiboo' ); ?>
                        </span>
                    </li>
                    <li <?php echo ( is_checkout() ? 'class="active"' : '' ) ?>>
                        <span>
                            <?php esc_html_e( 'Delivery and payment', 'woobiboo' ); ?>
                        </span>
                    </li>
                    <li <?php echo ( is_order_received_page() ? 'class="active"' : '' ) ?>>
                        <span>
                            <?php esc_html_e( 'Summary', 'woobiboo' ); ?>
                        </span>
                    </li>
                </ul>
            <?php
        endif;
	}



	/**
	 * Safe shipping checkout badge
	 */
	public function safe_shopping_badge() {
		if ( is_cart() || is_checkout() || is_order_received_page() ) :
            ?>
                <div class="safe-shopping">
	                <?php inline_svg(get_template_directory_uri() . '/assets/img/shield.svg' ); ?>
	                <?php esc_html_e( 'Safe shopping', 'woobiboo' ); ?>
                </div>
            <?php
        endif;
	}



	/**
	 * Facebook domain verification
	 */
	public function fb_domain_verification() {
	    ?>
            <meta name="facebook-domain-verification" content="0h0x8h1nm3wou08kvqr8ekogyl59o6" />
        <?php
	}


}
