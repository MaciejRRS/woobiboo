<?php
namespace WooBiBoo;

use WooBiBoo\Helpers\SvgSupport;

class Theme_Helpers {



    /**
     * Add SVG support for WP Media Library & uploader
     */
    public static function svg_support() {
        new SvgSupport();
    }



    /**
     * Adds class name to body created from post type and sanitized slug
     */
    public static function body_class() {
        add_filter( 'body_class', function( $classes ) {
            if ( is_single() ) :
                $classes[] = get_post_type() . '-' . sanitize_html_class( get_queried_object()->post_name );
            endif;
            return $classes;
        });
    }


}
