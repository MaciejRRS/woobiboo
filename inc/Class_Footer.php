<?php
namespace WooBiBoo;

use function WooBiBoo\Helpers\inline_svg;

/**
 * Theme Footer
 * Includes all methods required by theme header
 *
 * @since   3.0
 * @author  Mateusz Major
 * @link    https://inspirelabs.pl/
 * @package woobiboo
 */
class Class_Footer {

    public $footer;

	public function __construct() {
	    $this->footer = get_field( 'footer', 'option' );
		add_action( 'fh-site-footer', array( $this, 'top_decoration' ), 1 );
		add_action( 'fh-site-footer', array( $this, 'footer_header' ), 10 );
		add_action( 'fh-site-footer', array( $this, 'footer_widgets' ), 20 );
        add_action( 'fh-site-footer', array( $this, 'footer_switchers_open' ), 30 );
		add_action( 'fh-site-footer', array( $this, 'language_switcher' ), 33 );
		add_action( 'fh-site-footer', array( $this, 'currency_switcher' ), 34 );
		add_action( 'fh-site-footer', array( $this, 'footer_switchers_close' ), 39 );
		add_action( 'fh-site-footer', array( $this, 'footer_description' ), 40 );
		add_action( 'fh-site-footer', array( $this, 'bottom_decoration' ), 100 );
	}



	/**
	 * Colorful border decoration under main menu
	 */
	public function top_decoration() {
		if ( ! is_cart() && ! is_checkout() && ! is_order_received_page() ) :
            ?>
                <div class="woobiboo-border-decoration">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            <?php
        endif;
	}



	/**
	 * Show certificates and prizes
	 */
	public function prizes_certificates() {

		if ( ( $this->footer && $this->footer['certyficates'] && array_filter( $this->footer['certyficates'] ) ) && ( ! is_cart() && ! is_checkout() && ! is_order_received_page() ) ) :
            ?>
                <div class="prizes_certificates">
                    <?php
                        foreach ( $this->footer['certyficates'] as $certificate ) :
                            echo ( $certificate['url'] ? '<a href="' . esc_url( $certificate['url'] ) . '">' : '<span>' );
                            echo wp_get_attachment_image( $certificate['img']['ID'] );
                            echo ( $certificate['url'] ? '</a>' : '</span>' );
                        endforeach;
                    ?>
                </div>
            <?php
        endif;

	}



	/**
	 * Social links
	 */
	public function social_links() {

		if ( ( $this->footer && $this->footer['social_links'] && array_filter( $this->footer['social_links'] ) ) && ( ! is_cart() && ! is_checkout() && ! is_order_received_page() ) ) :
			?>
            <div class="social-links">
                <span><?php esc_html_e( 'Find us on', 'woobiboo' ); ?>:</span>
				<?php
				foreach ( $this->footer['social_links'] as $link ) :
					if ( $link['url'] ) :
						?>
                        <a href="<?php echo esc_url( $link['url'] ); ?>" target="_blank">
							<?php inline_svg( $link['icon']['url'] );?>
                        </a>
					<?php
					endif;
				endforeach;
				?>
            </div>
		<?php
		endif;
	}



	/**
	 * Footer header
	 */
	public function footer_header() {
		if ( ! is_cart() && ! is_checkout() && ! is_order_received_page() ) :
            ?>
            <div class="site-footer__header container">
                <?php if ( function_exists( 'the_custom_logo' ) ) : ?>
                    <?php the_custom_logo(); ?>
                <?php endif; ?>

                <?php
                    $this->social_links();
                    $this->prizes_certificates();
                ?>
            </div>
            <?php
        endif;
	}



	/**
	 * Footer widget area
	 */
	public function footer_widgets() {
		if ( ! is_cart() && ! is_checkout() && ! is_order_received_page() ) :
            ?>
                <div class="site-footer__navigation container">
                    <?php if ( is_active_sidebar( 'footer' ) ) : ?>
                        <?php dynamic_sidebar( 'footer' ); ?>
                        <?php $this->prizes_certificates(); ?>
                    <?php endif; ?>
                </div>
            <?php
        endif;
	}



	/**
	 * Footer switchers area open
	 */
	public function footer_switchers_open() {
		if ( $this->footer['description'] && ( ! is_cart() && ! is_checkout() && ! is_order_received_page() ) ) :
            ?>
                <div class="container footer__switcher">
            <?php
        endif;
	}



	/**
	 * Footer switchers area close
	 */
	public function footer_switchers_close() {
		if ( $this->footer['description'] && ( ! is_cart() && ! is_checkout() && ! is_order_received_page() ) ) :
            ?>
                </div>
            <?php
        endif;
	}



	/**
	 * Poolylang language switcher
	 */
	public function language_switcher() {
	    if ( function_exists('pll_the_languages') && ( ! is_cart() && ! is_checkout() && ! is_order_received_page() ) ) :
            ?>
                <div class="footer__switcher__wrapper">
                    <label for="lang_choice_1">
                        <?php esc_html_e('Choose language', 'woobiboo' ); ?>
                    </label>
                    <?php pll_the_languages( array( 'dropdown' => 1 ) ); ?>
                </div>
            <?php
	    endif;
	}



	/**
	 * Currency switcher
	 */
	public function currency_switcher() {
	    if ( shortcode_exists( 'woocommerce_currency_switcher_drop_down_box' ) && ( ! is_cart() && ! is_checkout() && ! is_order_received_page() ) ) :
            ?>
                <div class="footer__switcher__wrapper">
                    <label for="alg_currency_select">
                        <?php esc_html_e('Choose currency', 'woobiboo' ); ?>
                    </label>
                    <?php echo do_shortcode('[woocommerce_currency_switcher_drop_down_box]'); ?>
                </div>
            <?php
	    endif;
	}



	/**
	 * Site description with designer link
	 */
	public function footer_description() {
	    if ( $this->footer['description'] && ( ! is_cart() && ! is_checkout() && ! is_order_received_page() ) ) :
            ?>
            <div class="site-description container">
                <div class="description">
                    <?php
                    if ( function_exists('pll_current_language') && 'pl' != pll_current_language() && array_key_exists('description-' . pll_current_language(), $this->footer ) ) :
	                    echo $this->footer['description-' . pll_current_language() ];
                    else :
                        echo $this->footer['description'];
                    endif;
                    ?>
                </div>
                <div class="design">
                    <?php esc_html_e( 'Design', 'woobiboo' ); ?>
                    <a href="https://tomostudio.pl" target="_blank">
	                    <?php inline_svg(get_template_directory_uri() . '/assets/img/tomo.svg' ); ?>
                    </a>
                </div>
            </div>
            <?php
        endif;
	}



	/**
	 * Bottom cut through logo decoration
	 */
	public function bottom_decoration() {
		if ( ! is_cart() && ! is_checkout() && ! is_order_received_page() ) :
            ?>
                <img class="site-footer__decoration" src="<?php echo get_template_directory_uri() . '/assets/img/footer-logo.png' ?>" width="4317" height="324">
            <?php
        endif;
    }



}
