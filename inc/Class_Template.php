<?php
namespace WooBiBoo;

/**
 * Common theme templates
 *
 * @package woobiboo
 * @author  Mateusz Major
 * @link    https://inspirelabs.pl/
 * @since   3.0
 */
class Class_Template {

	public function __construct() {
		add_action( 'fh-page-content', array( $this, 'page_title' ), 10 );
		add_action( 'fh-page-content', array( $this, 'page_content' ), 20 );
		add_filter( 'body_class', array( $this, 'body_classes' ) );
		add_filter( 'wpcf7_autop_or_not', '__return_false' );

	}

	public function page_title() {
		if ( ! is_account_page() && ! is_user_logged_in() ) : // Remove title from user account login page
			the_title( '<h1>', '</h1>' );
		endif;
	}

	public function page_content() {
		the_content();
	}

	public function body_classes( $classes ) {

		if ( is_account_page() && ! is_user_logged_in() ) :
			$classes[] = 'woocommerce_login_page';
		endif;

		if ( is_checkout() ) :
			$classes[] = 'woocommerce_checkout_page';
		endif;

		if ( is_cart() ) :
			$classes[] = 'woocommerce_cart_page';
		endif;

		if ( is_page( 'shop' ) ) :
			$classes[] = 'archive';
		endif;

		return $classes;

	}





}