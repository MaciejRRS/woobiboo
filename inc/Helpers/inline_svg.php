<?php
namespace WooBiBoo\Helpers;

/**
 * Puts SVG inline from given file
 *
 * @param   string $path path to svg file.
 * @return  string $path included file content.
 * @package woobiboo
 * @author  Mateusz Major
 * @since   1.0
 */
function inline_svg( $path, $echo = true ) {

	if ( 200 === wp_remote_retrieve_response_code( wp_remote_get( $path ) ) ) :  // Check if file exists.

		$file_extension    = pathinfo( $path, PATHINFO_EXTENSION );
		$allowed_extension = array( 'jpg', 'jpeg', 'jpe', 'gif', 'png', 'bmp', 'svg', 'svgz', 'webp' );

		if ( in_array( $file_extension, $allowed_extension ) ) : // Check if extension is allowed.

			$file = false;

	        if ( 'svg' === pathinfo( $path, PATHINFO_EXTENSION ) ) :
		        $file = file_get_contents( $path );
		    else :
		        $file = ' <img src="'. esc_url( $path ) . '">';
	        endif;

			if ( $echo ) : // Return string or echo it.
				echo $file;
			else :
				return $file;
			endif;

        endif;

    endif;

}