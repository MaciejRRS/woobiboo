<?php
namespace WooBiBoo\Helpers;

class SvgSupport {



    public function __construct() {
        add_filter( 'wp_check_filetype_and_ext', array( $this, 'svg_upload' ), 10, 4 );
        add_filter( 'upload_mimes', array( $this, 'svg_mime_type' ) );
    }



    /**
     * Add svg mime types to supported
     */
    public function svg_mime_type( $mimes ) {

        $mimes['svg']  = 'image/svg+xml';
        $mimes['svgz'] = 'image/svg+xml';

        return $mimes;

    }



    /**
     * Adds support for svg file upload
     */
    public function svg_upload( $data, $file, $filename, $mimes ) {

        global $wp_version;

        if ( '4.7.1' === $wp_version ) {
            return $data;
        }

        $filetype = wp_check_filetype( $filename, $mimes );

        return array(
            'ext'             => $filetype['ext'],
            'type'            => $filetype['type'],
            'proper_filename' => $data['proper_filename'],
        );
    }


}
