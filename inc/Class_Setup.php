<?php
namespace WooBiBoo;

/**
 * Theme setup
 * Includes assets, CPT, CT, theme support
 *
 * @package woobiboo
 * @author  Mateusz Major
 * @link    https://inspirelabs.pl/
 * @since   2.0
 */
class Class_Setup {

	public function __construct() {
        add_action( 'init', array( $this, 'register_menus' ) );
        add_action( 'widgets_init', array( $this, 'register_sidebars' ), 11 );
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_assets' ) );
		add_action( 'after_setup_theme', array( $this, 'woocommerce_product_gallery' )  );

        // Add support
        add_theme_support( 'title-tag' );
        add_theme_support( 'custom-logo' );
        add_theme_support( 'editor-styles' );
        add_theme_support( 'woocommerce' );
        add_post_type_support( 'page', 'excerpt' );
		add_filter( 'widget_text', 'do_shortcode' );

        // Add ACF Options Page - Theme Settings
		add_action( 'acf/init', array( $this, 'theme_options_page' ) );

		// Theme translation
		add_action( 'after_setup_theme', array( $this, 'theme_translation' ) );

		// Theme uri
		add_filter('template_directory_uri', array( $this, 'override_theme_uri' ), 10, 2);
	}



	/**
	 * Register menu
	 */
	public function register_menus() {
		register_nav_menus( array( 'primary' => 'Primary' ) );
        register_nav_menus( array( 'secondary' => 'Secondary' ) );
        register_nav_menus( array( 'mobile' => 'Mobile' ) );
	}



	/**
	 * Register widget areas
	 */
	public function register_sidebars() {
		// Footer.
		register_sidebar( array(
			'name'          => esc_html__( 'Footer', 'woobiboo' ),
			'id'            => 'footer',
			'before_widget' => '<div class="widget">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4>',
			'after_title'   => '</h4>',
		));
		// Mini cart.
		register_sidebar( array(
			'name'          => 'Mini cart',
			'id'            => 'mini_cart',
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '<span style="display: none">',
			'after_title'   => '</span>',
		) );
	}



	/**
	 * Enqueue CSS & JS
	 */
	public function enqueue_assets() {

	    // CSS
		wp_enqueue_style( 'google-fonts', '  https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,400;0,500;0,700;1,400&display=swap');
		wp_enqueue_style( 'main', get_template_directory_uri() . '/assets/css/main.css', array (), wp_get_theme()->version );

		// JS
		wp_enqueue_script( 'scripts', get_template_directory_uri() . '/assets/js/scripts.js', array(), wp_get_theme()->version, true );

	}



	/**
	 * Create ACF Option Page for theme settings
	 *
	 * @return void
	 */
	public function theme_options_page() {

		if ( function_exists( 'acf_add_options_page' ) ) :

			$arg = array(
				'page_title'  => __( 'Theme Settings', 'woobiboo' ),
				'menu_title'  => __( 'Theme Settings', 'woobiboo' ),
				'parent_slug' => 'themes.php',
				'capability'  => 'edit_posts',
				'redirect'    => false,
			);

			// Register options page.
			$option_page = acf_add_options_page( $arg );

		endif;
	}



	/**
	 * Load theme translation
	 */
	public function theme_translation() {
		load_theme_textdomain( 'woobiboo', get_template_directory() . '/languages' );
	}



	/**
	 * Override wrong theme uri caused by Polylang multiple domains
	 */
	public function override_theme_uri( $url ) {

		$needle = 'woobiboo.pl';

		if( strpos( $url, $needle ) !== false ) :
			$url = str_replace( $needle, 'woobiboo.com', $url );
		endif;

		return $url;
	}


	public function woocommerce_product_gallery() {
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );
	}

}