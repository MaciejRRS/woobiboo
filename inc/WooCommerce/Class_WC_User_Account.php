<?php
namespace WooBiBoo\WooCommerce;

use WooBiBoo\Class_Template;
use function WooBiBoo\Helpers\inline_svg;

/**
 * WooCommerce User Account Page
 *
 * @package woobiboo
 * @author  Mateusz Major
 * @link    https://inspirelabs.pl/
 * @since   3.0
 */
class Class_WC_User_Account {

	/**
	 * Class_General constructor.
	 */
	public function __construct() {
		add_action( 'woocommerce_before_lost_password_form', array( $this, 'lost_password_title' ) );
    }

    public function lost_password_title() {
		?>
	        <h2>
		        <?php esc_html_e( 'Lost password', 'woobiboo' ); ?>
	        </h2>
		<?php
    }



}
