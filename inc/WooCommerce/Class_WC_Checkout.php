<?php
namespace WooBiBoo\WooCommerce;

use function WooBiBoo\Helpers\inline_svg;

/**
 * WooCommerce Checkout
 *
 * @package woobiboo
 * @author  Mateusz Major
 * @link    https://inspirelabs.pl/
 * @since   3.0
 */
class Class_WC_Checkout {

	/**
	 * Class_General constructor.
	 */
	public function __construct() {

		remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10);
		remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10);

		add_action( 'woocommerce_checkout_cart_review', array( $this, 'order_content' ), 1 );
		add_action( 'woocommerce_review_order_before_shipping', array( $this, 'shipping_title' ), 10 );

		add_action( 'woocommerce_checkout_after_order_review', array( $this, 'confirm_order' ), 999 );
		add_action( 'woocommerce_checkout_after_order_review', array( $this, 'safety_certificate_description' ), 90 );
		add_action( 'woocommerce_checkout_after_order_review', array( $this, 'checkout_footer_actions' ), 999 );
    }



	/**
	 * Shipping title
	 */
    public function shipping_title() {
	    ?>
            <h3><?php esc_html_e( 'Choose a shipping method', 'woobiboo' ); ?></h3>
        <?php
    }



	/**
	 * Order review
	 */
	public function order_content() {
	    ?>
            <div class="order-content">
                <h3><?php esc_html_e( 'Your order', 'woobiboo' ); ?></h3>
                <?php do_action( 'woocommerce_review_order_before_cart_contents' ); ?>
                <div class="shop-table cart">
                    <?php
                    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                        $_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

                        if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                        ?>
                        <div class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart__item', $cart_item, $cart_item_key ) ); ?>">
                            <div class="product-thumbnail">
		                        <?php echo apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key ); ?>
                            </div>
                            <div class="product-name">
	                            <?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity', ' <strong class="product-quantity">' . sprintf( '%s&nbsp;&times;&nbsp;', $cart_item['quantity'] ) . '</strong>', $cart_item, $cart_item_key ); ?>
                                <?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;'; ?>
                                <?php echo wc_get_formatted_cart_item_data( $cart_item ); ?>
                            </div>
                            <div class="product-total">
                                <?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
                            </div>
                        </div>
                        <?php
                        }
                    }
                    ?>
                </div>
	            <?php do_action( 'woocommerce_review_order_after_cart_contents' ); ?>
            </div>
	    <?php
    }



	/**
	 * Sticky confirm order element
	 */
    public function confirm_order() {
	    if ( ! is_ajax() ) {
		    do_action( 'woocommerce_review_order_before_payment' );
	    }
	    $order_button_text = __( 'I buy', 'woobiboo' );
		?>
		    <div id="create-order" class="form-row place-order">
			    <div class="place-order__wrapper">
				    <noscript>
					    <?php
					    /* translators: $1 and $2 opening and closing emphasis tags respectively */
					    printf( esc_html__( 'Since your browser does not support JavaScript, or it is disabled, please ensure you click the %1$sUpdate Totals%2$s button before placing your order. You may be charged more than the amount stated above if you fail to do so.', 'woocommerce' ), '<em>', '</em>' );
					    ?>
					    <br/><button type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="<?php esc_attr_e( 'Update totals', 'woocommerce' ); ?>"><?php esc_html_e( 'Update totals', 'woocommerce' ); ?></button>
				    </noscript>

				    <?php wc_get_template( 'checkout/terms.php' ); ?>

				    <?php do_action( 'woocommerce_review_order_before_submit' ); ?>

				    <?php echo apply_filters( 'woocommerce_order_button_html', '<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '">' . esc_html( $order_button_text ) . '</button>' ); // @codingStandardsIgnoreLine ?>

				    <?php do_action( 'woocommerce_review_order_after_submit' ); ?>

				    <?php wp_nonce_field( 'woocommerce-process_checkout', 'woocommerce-process-checkout-nonce' ); ?>
				    </div>
		    </div>
		<?php
	    if ( ! is_ajax() ) {
		    do_action( 'woocommerce_review_order_after_payment' );
	    }
    }



	/**
	 * Additional actions under cart shop table
	 */
	public function checkout_footer_actions() {
		?>
        <footer class="checkout_footer">
            <a href="<?php echo esc_url( wc_get_cart_url() ) ?>" class="button-text button-back">
				<?php inline_svg(get_template_directory_uri() . '/assets/img/arrow-left.svg' ); ?>
				<?php esc_html_e( 'Go back', 'woobiboo' ); ?>
            </a>
        </footer>
		<?php
	}



	/**
	 * Additional actions under cart shop table
	 */
	public function safety_certificate_description() {
		$checkout = get_field('checkout' , 'options' );
		if ( $checkout && isset( $checkout['security_text'] ) ) :
			?>
            <div class="shopping-safety">
				<?php
                    if ( function_exists('pll_current_language') && 'pl' != pll_current_language() && array_key_exists('security_text-' . pll_current_language(), $checkout ) ) :
                        echo wp_kses_post( $checkout['security_text-' . pll_current_language() ] );
                    else :
                        echo wp_kses_post( $checkout['security_text'] );
                    endif;
				?>
            </div>
		<?php
		endif;
	}


}
