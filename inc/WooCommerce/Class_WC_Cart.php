<?php
namespace WooBiBoo\WooCommerce;

use function WooBiBoo\Helpers\inline_svg;

/**
 * WooCommerce Cart
 *
 * @package woobiboo
 * @author  Mateusz Major
 * @link    https://inspirelabs.pl/
 * @since   3.0
 */
class Class_WC_Cart {

	/**
	 * Class_General constructor.
	 */
	public function __construct() {

		add_filter( 'woocommerce_cart_item_remove_link', array( $this, 'replace_remove_from_cart' ), 10, 2 );
		remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');
		remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cart_totals', 10);
		add_action( 'woocommerce_cart_collaterals', array( $this, 'cart_totals_wrapper_open' ), 10 ); // WooCommerce cart totals wrapper open.
		add_action( 'woocommerce_cart_collaterals', array( $this, 'cart_totals_wrapper_close' ), 999 ); // WooCommerce cart totals wrapper close.
		add_action( 'woocommerce_cart_collaterals', array( $this, 'cart_totals' ), 20 ); // Custom WooCommerce cart totals.
		add_action( 'woocommerce_cart_collaterals', array( $this, 'cart_shop_features' ), 30 ); // Custom WooCommerce cart totals.
        add_action( 'woocommerce_after_cart_table', array( $this, 'cart_footer_actions' ) );
        add_action( 'woocommerce_cart_collaterals', array( $this, 'cart_footer_actions_mobile' ), 9999 );
		add_filter( 'woocommerce_checkout_coupon_message', '__return_false' ); // Remove coupon form trigger.

		add_filter( 'wc_add_to_cart_message_html', '__return_false' );
		add_action( 'woocommerce_add_to_cart', array( $this, 'action_woocommerce_add_to_cart' ), 10, 3 );

	}



	/**
	 * Replace default WooCommerce cart remove item link
	 *
	 * @param string $sprintf
	 * @param string $cart_item_key WC cart item key.
	 * @return string $sprintf
	 */
	public function replace_remove_from_cart($sprintf, $cart_item_key ) {

		if ( \WC()->cart->get_cart()[$cart_item_key] ) :
			$sprintf = sprintf(
				'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">%s</a>',
				esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
				esc_html__( 'Remove this item', 'woocommerce' ),
				esc_attr( \WC()->cart->get_cart()[$cart_item_key]['product_id'] ),
				esc_attr( \WC()->cart->get_cart()[$cart_item_key]['data']->get_sku() ),
				__( 'Remove', 'woobiboo' )
			);
		endif;

	    return $sprintf;
    }



    public function cart_totals_wrapper_open() {
	    ?>
            <div class="cart-collaterals-wrapper">
        <?php
    }



    public function cart_totals_wrapper_close() {
	    ?>
            </div>
        <?php
    }



	/**
	 * Cart totals
	 */
	public function cart_totals() {
		?>
			<div class="cart_totals">
				<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
                    <div class="cart_totals_cupon">
                        <?php wc_cart_totals_coupon_label( $coupon ); ?> <?php wc_cart_totals_coupon_html( $coupon ); ?>
                    </div>
				<?php endforeach; ?>
				<div class="cart_totals_price">
					<?php esc_html_e( 'To pay', 'woobiboo' ); ?>
					<?php echo \WC()->cart->get_cart_total(); ?>
				</div>
				<div class="wc-proceed-to-checkout">
					<a href="<?php echo esc_url( wc_get_checkout_url() ) ?>" class="checkout-button button alt wc-forward"><?php esc_html_e( 'Go to checkout', 'woobiboo' ); ?></a>
				</div>
			</div>
	    <?php
    }



	/**
	 * Features shown inside cart sidebar
	 */
    public function cart_shop_features() {
	    $features =  get_field( 'cart', 'option' );

	    if ( $features && array_filter( $features['features'] ) ) :
		    ?>
            <div class="why-us">
                <ul class="list">
				    <?php
				    foreach ( $features['features'] as $feature ) :
					    ?>
                        <li class="list__item">
						    <?php
                                if ( $feature['icon'] ) :
                                    echo wp_get_attachment_image( $feature['icon']['id'], 'full' );
                                endif;
                                if ( function_exists('pll_current_language') && 'pl' != pll_current_language() && array_key_exists('description-' . pll_current_language(), $feature ) ) :
                                    echo esc_html( $feature['description-' . pll_current_language() ] );
                                else :
                                    echo esc_html( $feature['description'] );
                                endif;
						    ?>
                        </li>
				    <?php
				    endforeach;
				    ?>
                </ul>
            </div>
	    <?php
	    endif;
    }



	/**
	 * Additional actions under cart shop table
	 */
    public function cart_footer_actions() {
	    ?>
            <footer class="cart_footer">
                <a href="javascript:history.back()" class="button-text button-back">
	                <?php inline_svg(get_template_directory_uri() . '/assets/img/arrow-left.svg' ); ?>
                    <?php esc_html_e( 'Go back', 'woobiboo' ); ?>
                </a>

                <button class="button-text button-show-coupon-form" onclick="showCouponForm()" type="button">
	                <?php inline_svg(get_template_directory_uri() . '/assets/img/tag.svg' ); ?>
                    <?php esc_html_e('Do you have a discount code?', 'woobiboo'); ?>
                </button>

                <div id="coupon-form">
                    <?php woocommerce_checkout_coupon_form(); ?>
                </div>
            </footer>
        <?php
    }

	/**
	 * Additional actions under cart shop table
	 */
    public function cart_footer_actions_mobile() {
	    ?>
            <footer class="cart_footer">
                <a href="javascript:history.back()" class="button-text button-back">
	                <?php inline_svg(get_template_directory_uri() . '/assets/img/arrow-left.svg' ); ?>
                    <?php esc_html_e( 'Go back', 'woobiboo' ); ?>
                </a>

                <button class="button-text button-show-coupon-form button-show-coupon-form-mobile" type="button">
	                <?php inline_svg(get_template_directory_uri() . '/assets/img/tag.svg' ); ?>
                    <?php esc_html_e('Do you have a discount code?', 'woobiboo'); ?>
                </button>

                <div id="coupon-form-mobile">
                    <?php woocommerce_checkout_coupon_form(); ?>
                </div>
            </footer>
        <?php
    }



	/**
	 * Added to cart popup
	 */
    public function action_woocommerce_add_to_cart() {
	    add_action( 'wp_footer', function() {
	        ?>
                <div id="add_to_cart_popup" class="add_to_cart_popup__overlay is-visible">
                    <section class="add_to_cart_popup">
                        <button class="popup-close" onclick="closeAddToCartPopUp(this)" type="button">
                            <?php inline_svg(get_template_directory_uri() . '/assets/img/close.svg' ); ?>
                        </button>
                        <?php
                        $cart_item = WC()->cart->get_cart();
                        $cart_item_key = array_key_last( $cart_item );
                        $cart_item = end( $cart_item );

                            $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                            $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

                            if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) :
                                $product_name      = apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key );
                                $thumbnail         = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image('large'), $cart_item, $cart_item_key );
                                $product_price     = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
                                $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                                ?>
                                <div class="woocommerce-mini-cart-item <?php echo esc_attr( apply_filters( 'woocommerce_mini_cart_item_class', 'mini_cart_item', $cart_item, $cart_item_key ) ); ?>">
                                    <div class="woocommerce-mini-cart-item__image">
                                        <?php echo $thumbnail; ?>
                                    </div>
                                    <div class="woocommerce-mini-cart-item__description">
                                        <span class="heading">
                                            <?php esc_html_e( 'The product has been added to the cart', 'woobiboo' ); ?>
                                        </span>
                                        <h2>
                                            1 &times; <?php echo $product_name; ?>
                                        </h2>
                                        <?php echo wc_get_formatted_cart_item_data( $cart_item ); ?>

                                        <a class="button button-go-to-cart" href="<?php echo esc_url( wc_get_cart_url() ); ?>"><?php esc_html_e( 'Go to cart', 'woobiboo' ); ?></a>
                                        <button class="button-text button-continue" onclick="closeAddToCartPopUp(this)" type="button"><?php esc_html_e( 'Continue shopping', 'woobiboo' ); ?></button>

                                    </div>
                                </div>
                            <?php
                        endif;
                        ?>
                    </section>
                </div>
            <?php
        } );
    }

}
