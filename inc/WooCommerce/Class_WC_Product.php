<?php
namespace WooBiBoo\WooCommerce;

use function WooBiBoo\Helpers\inline_svg;

/**
 * WooCommerce Single Product functions
 *
 * @package woobiboo
 * @author  Mateusz Major
 * @link    https://inspirelabs.pl/
 * @since   3.0
 */
class Class_WC_Product {

    public function __construct() {

    	// Remove default WooCommerce wrapper.
	    remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
	    remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

	    // Product details.
	    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
	    remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
	    remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );

	    add_action( 'woocommerce_before_single_product_summary', array( $this, 'product_details_wrapper_open' ), 30 );
	    add_action( 'woocommerce_before_single_product_summary', array( $this, 'product_description' ), 40 );
	    add_action( 'woocommerce_before_single_product_summary', array( $this, 'product_specification' ), 50 );
	    add_action( 'woocommerce_before_single_product_summary', array( $this, 'product_colors' ), 60 );
	    add_action( 'woocommerce_before_single_product_summary', array( $this, 'product_includes' ), 70 );
	    add_action( 'woocommerce_before_single_product_summary', array( $this, 'product_details_wrapper_close' ), 999 );

	    // Product aside.
	    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
	    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
	    add_action( 'woocommerce_single_product_summary', array( $this, 'product_summary_sticky_open' ), 1 );
	    add_action( 'woocommerce_single_product_summary', 'woocommerce_show_product_sale_flash', 3 );
	    add_action( 'woocommerce_single_product_summary', array( $this, 'product_summary_features' ), 1000 );
	    add_action( 'woocommerce_single_product_summary', array( $this, 'product_rating' ), 1001 );
	    add_action( 'woocommerce_single_product_summary', array( $this, 'product_summary_sticky_close' ), 999 );
	    add_filter('woocommerce_reset_variations_link', '__return_empty_string'); // Remove reset variation link.

	    remove_action( 'woocommerce_single_variation', 'woocommerce_single_variation', 10 ); // Remove variation price AFTER select.
	    add_action( 'woocommerce_before_variations_form', 'woocommerce_single_variation', 10 ); // Add  variation price BEFORE select.
	    add_filter( 'woocommerce_variable_sale_price_html',  array( $this, 'remove_variable_product_price' ), 10, 2 ); // Remove main price to prevent price duplication with variable.
	    add_filter( 'woocommerce_variable_price_html',  array( $this, 'remove_variable_product_price' ), 10, 2 ); // Remove main price to prevent price duplication with variable.
	    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart'); // Remove add to cart button from related products.

        // Related products
	    add_filter( 'woocommerce_product_related_products_heading', array( $this, 'related_products_header' ) );
	    add_filter( 'woocommerce_related_products', array( $this, 'related_products') , 9999, 3 );
    }



	/**
	 * Open wrapper for product descriptions, colors etc.
	 */
	public function product_details_wrapper_open() {
    	?>
<div class="product__details">
    <?php
    }



	/**
	 * Open wrapper for product descriptions, colors etc.
	 */
    public function product_details_wrapper_close() {
    	?>
</div>
<?php
    }



	/**
	 *  Add section with full product description
	 */
	public function product_description() {

    	global $product;
    	if ( $product->get_description() ) :
		    ?>
<section class="section section--description">
    <h2>
        <?php esc_html_e( 'Description', 'woobiboo' ) ?>
    </h2>
    <?php echo $product->get_description() ?>
</section>
<?php
	    endif;
    }



	/**
	 * Add section with product specification
	 */
	public function product_specification() {

		global $product;

		$attributes =  $product->get_attributes();
		if ( $attributes ) :

			$attributes_visible = array();
			array_walk($attributes, function( &$attribute, &$id) use (&$attributes_visible) {
				if ( $attribute->get_visible() ) :
					$attributes_visible[$id] = $attribute;
				endif;
			});

			if ( $attributes_visible ) :
				?>
<section class="section section--specification">
    <h2>
        <?php esc_html_e( 'Specification', 'woobiboo' ) ?>
    </h2>
    <dl class="list">
        <?php
                            foreach ( $attributes_visible as $name => $attribute ) :
                                ?>
        <dt class="list__item-name"><?php esc_html_e( $attribute->get_name() ); ?></dt>
        <dd class="list__item-value"><?php esc_html_e( implode( ', ', $attribute->get_options() ) ); ?></dd>
        <?php
                            endforeach;
						?>
    </dl>
</section>
<?php
			endif;

		endif;
	}



	/**
	 *  Add section with full product colors
	 */
	public function product_colors() {

		$colors = get_field( 'color_scheme', get_the_ID() );
		if ( $colors && array_filter( $colors ) ) :
			?>
<section class="section section--colors">
    <h2>
        <?php esc_html_e( 'Color scheme', 'woobiboo' ) ?>
    </h2>
    <dl class="list">
        <?php foreach ( $colors as $item ) : ?>
        <dt class="list__name"><?php echo esc_html( $item['name'] ); ?></dt>
        <dd class="list__colors">
            <?php
                                if ( $item['color'] ) :
                                    foreach ( $item['color'] as $color ) :
                                        ?>
            <div class="item" style="background-color: <?php echo esc_attr( $color['hex'] ); ?>"></div>
            <?php
                                    endforeach;
                                endif;
                            ?>
        </dd>
        <?php endforeach; ?>
    </dl>
</section>
<?php
        endif;

	}



	/**
	 *  Add section with full product elements
	 */
    public function product_includes() {

		$includes = get_field( 'elementy', get_the_ID() );

	    if ( $includes ) :
		    ?>
<section class="section section--includes">
    <h2>
        <?php esc_html_e( 'Elements', 'woobiboo' ) ?>
    </h2>
    <?php echo $includes ?>
</section>
<?php
	    endif;

    }



	/**
	 * Open wrapper for product descriptions, colors etc.
	 */
	public function product_summary_sticky_open() {
		?>
<div class="product__summary__sticky">
    <?php
	}



	/**
	 * Product rating.
	 */
	
	public function product_rating() {
		
		$review_settings = get_option( 'woocommerce_enable_reviews' );
		if ( $review_settings === 'yes' ) :
            global $product;
            ?>
    <section class="product-rating">
        <header>
            <h4>
                <?php esc_html_e( 'Product reviews', 'woobiboo' ) ?>
            </h4>
        </header>
        <div class="wrapper">
            <div class="rating">
                <?php
                $countReviews = $product->get_rating_count();
                if ($countReviews < 1 ) { ?>
                <span class="average"><?php esc_html_e( 'Brak', 'woocommerce' ); ?></span>

                <?php }
                else { ?>

                <span class="average">
                    <?php echo $product->get_average_rating(); ?>
                </span>
                /5
                <?php } ?>
            </div>

            <?php
                $countReviews = $product->get_rating_count();
                if ($countReviews > 0 ) { ?>
            (<?php echo $product->get_rating_count(); ?>)

            <?php   }   else {    } ?>

            <ul class="rating-stars rating-<?php echo round( $product->get_average_rating() ) ?>">
                <li>
                    <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/star.svg' ); ?>
                </li>
                <li>
                    <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/star.svg' ); ?>
                </li>
                <li>
                    <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/star.svg' ); ?>
                </li>
                <li>
                    <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/star.svg' ); ?>
                </li>
                <li>
                    <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/star.svg' ); ?>
                </li>
            </ul>
        </div>



        <?php
                $countReviews = $product->get_rating_count();
                if ($countReviews > 0 ) { ?>
        <button id="myBtn" type="button">
            <?php the_field('tekst_przycisku_czytaj_opinie','options') ?>
        </button>

        <?php   }   else {  ?>

        <button id="myBtn" type="button">
            <?php the_field('tekst_przycisku_dodaj_opinie','options') ?>
        </button>

        <?php     } ?>



    </section>
    <?php
    
        endif;
	}



	/**
	 * Open wrapper for product descriptions, colors etc.
	 */
	public function product_summary_sticky_close() {
		?>
</div>





<!-- MIDAL REVIEWS START -->

<style>
/* The Modal (background) */
.modal {
    display: none;
    /* Hidden by default */
    position: fixed;
    /* Stay in place */
    z-index: 9999;
    /* Sit on top */
    padding-top: 100px;
    /* Location of the box */
    left: 0;
    top: 0;
    width: 100%;
    /* Full width */
    height: 100%;
    /* Full height */
    overflow: auto;
    /* Enable scroll if needed */
    background-color: rgb(0, 0, 0);
    /* Fallback color */
    background-color: rgba(0, 0, 0, 0.4);
    /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    position: relative;
    background-color: #fefefe;
    padding: 0;
    border: 1px solid #888;
    max-width: 880px;
    margin: 0 auto;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s;

}

@media(max-width:1199px) {
    .modal-content {
        max-width: calc(100% - 30px);
    }
}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {
        top: -300px;
        opacity: 0
    }

    to {
        top: 0;
        opacity: 1
    }
}

@keyframes animatetop {
    from {
        top: -300px;
        opacity: 0
    }

    to {
        top: 0;
        opacity: 1
    }
}

/* The Close Button */
.close {
    font-size: 45px;
    font-weight: 300;
    position: absolute;
    right: 20px;
    top: 0;
    color: #355E81
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

.modal-header {
    padding: 2px 16px;
    background-color: #5cb85c;
    color: white;
}

.modal-body {
    padding: 2px 16px;
}

.modal-footer {
    padding: 2px 16px;
    background-color: #5cb85c;
    color: white;
}
</style>



<!-- The Modal -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close" id="close">&times;</span>
        <?php comments_template(); ?>
    </div>

</div>

<!-- MODAL REVIEWS END -->









<?php
	}


	/**
	 * Aside section "Why us?"
	 */
	public function product_summary_features() {

	    $features =  get_field( 'product', 'option' );

		if ( $features && array_filter( $features['features'] ) ) :
            ?>
<div class="why-us">
    <ul class="list">
        <?php
                    foreach ( $features['features'] as $feature ) :
                        ?>
        <li class="list__item">
            <?php
                                if ( $feature['icon'] ) :
	                                echo wp_get_attachment_image( $feature['icon']['id'], 'full' );
                                endif;
                            ?>
            <?php
                                if ( function_exists('pll_current_language') && 'pl' != pll_current_language() && array_key_exists('description-' . pll_current_language(), $feature ) ) :
                                    echo esc_html( $feature['description-' . pll_current_language() ] );
                                else :
                                    echo esc_html( $feature['description'] );
                                endif;
	                        ?>
        </li>
        <?php
                    endforeach;
                    ?>
    </ul>
</div>
<?php
        endif;
	}



	/**
     * Remove product price duplicate from variable with different prices
     *
	 * @param float $price
	 * @param object $product
	 * @return float|bool $price
	 */
	public function remove_variable_product_price( $price, $product ) {

		global $woocommerce_loop;

		if ( is_product() && empty( $woocommerce_loop['name'] ) && 'variable' === $product->get_type() && ( $product->get_variation_price( 'max', true ) != $product->get_variation_price( 'min', true ) ) ) :
			return false;
        else:
	        return $price;
        endif;

	}



	/**
     * Related products header
     *
	 * @param string $header
	 * @return string $header
	 */
	public function related_products_header($header ) {
	    return __( 'Check out our other products:', 'woobiboo' );
	}



	/**
     * Filter through related products and exclude with other language than currently selected
     *
	 * @param array $related_posts
	 * @param int $product_id
	 * @param array $args
	 * @return array $related_posts
	 */
	public function related_products($related_posts, $product_id, $args ) {

	    if ( function_exists('pll_get_post_language' ) ) :

            $current_language = pll_current_language();

		    $related_posts = array_filter( $related_posts, function( $post_id ) use ( $current_language ) {
		        if ( pll_get_post_language( $post_id ) === $current_language ) :
                    return $post_id;
                endif;
		    });

        endif;

        return $related_posts;

	}

}