<?php
namespace WooBiBoo\WooCommerce;

use function WooBiBoo\Helpers\inline_svg;

/**
 * WooCommerce Global setup
 *
 * @since   3.0
 *@author  Mateusz Major
 * @link    https://inspirelabs.pl/
 * @package woobiboo
 */
class Class_WC_Global {

	/**
	 * Class_General constructor.
	 */
	public function __construct() {

		// Remove all WooCommerce styles.
		add_filter( 'woocommerce_enqueue_styles', '__return_false' );

    	// Show single price for product with variable
	    add_filter('woocommerce_variable_sale_price_html', array( $this, 'shop_variable_product_price' ), 10, 2);
	    add_filter('woocommerce_variable_price_html',  array( $this, 'shop_variable_product_price' ), 10, 2 );

	    // Format product price
		add_filter( 'woocommerce_get_price_html', array( $this, 'format_product_price_display' ) );
		add_filter( 'woocommerce_cart_item_price', array( $this, 'format_product_price_display' ) );


	    // Disable quantity field
	    add_filter( 'woocommerce_is_sold_individually', array( $this, 'remove_all_quantity_fields' ), 10, 2 );

	    // Remove short description field from admin
		add_action('add_meta_boxes', array( $this, 'remove_short_description' ), 999);

		// Change default breadcrumbs separator.
		add_filter( 'woocommerce_breadcrumb_defaults', array( $this, 'change_breadcrumbs_separator') );
    }



	/**
	 * Single price for product with variables
	 *
	 * @param $price
	 * @param $product
	 * @return mixed|string
	 */
	public function shop_variable_product_price($price, $product ) {

		$variation_min_reg_price = $product->get_variation_regular_price( 'min', true );
		$variation_min_sale_price = $product->get_variation_sale_price( 'min', true );

		if ( $product->is_on_sale() && !empty( $variation_min_sale_price ) ) :
			if ( ! empty( $variation_min_sale_price ) ) :
				$price =  ' <del class="strike">' .  wc_price($variation_min_reg_price) . '</del>' . wc_price($variation_min_sale_price);
			endif;
		else :
			if ( ! empty( $variation_min_reg_price ) ) :
				$price = wc_price( $variation_min_reg_price );
			else :
				$price = wc_price( $product->regular_price );
			endif;
		endif;
		return $price;
	}



	/**
	 * Format WooCommerce product price and remove <ins> tag
	 *
	 * @param string $price WooCommerce product price as html
	 * @return string $price WooCommerce product price as html
	 */
	public function format_product_price_display( $price ) {
		$price = str_replace( array( '<ins>', '</ins>' ), '', $price );
		return $price;
	}



	/**
	 * Remove all quantity inputs
	 *
	 * @param $return
	 * @param $product
	 * @return bool
	 */
	public function remove_all_quantity_fields($return, $product ) {
		if ( is_product() ) :
			return true;
		else :
			return $return;
		endif;
	}



	/**
	 * Remove product short description
	 */
	public function remove_short_description() {
		remove_meta_box( 'postexcerpt', 'product', 'normal');
	}



	/**
	 * Change default Woocommerce breadcrumbs separator
	 */
	public function change_breadcrumbs_separator( $defaults ) {
		$defaults['delimiter'] = ' <i class="icon chevron-right"></i>';
		return $defaults;
	}

}
