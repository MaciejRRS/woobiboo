<?php
namespace WooBiBoo\WooCommerce;

use function WooBiBoo\Helpers\inline_svg;

/**
 * WooCommerce Product, Category archive
 *
 * @package woobiboo
 * @author  Mateusz Major
 * @link    https://inspirelabs.pl/
 * @since   3.0
 */
class Class_WC_Archive {

    public function __construct() {

	    remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 ); // Remove container - open.
	    add_action( 'woocommerce_before_main_content', array( $this, 'archive_container_open' ), 10 ); // Add new container - open.
	    remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 ); // Remove container - close.
	    add_action( 'woocommerce_after_main_content', array( $this, 'archive_container_close' ), 999 ); // Add new container - close.
	    remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 ); // Remove result count.
	    add_action( 'woocommerce_after_main_content', array( $this, 'category_long_description' ), 50 ); // Category long desription.
        remove_all_actions( 'woocommerce_sidebar' ); // Remove archive sidebar.

	    remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 ); // Remove default thumbnail just to add wrapper to it.
	    add_action( 'woocommerce_before_shop_loop_item_title', array( $this, 'product_thumbnail' ), 10 ); // Add wrapper to the thumbnail.
		remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 ); // Remove sorting.
	    add_action('pre_get_posts', array( $this, 'display_products_shop_page' ) );

	    remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 30 ); // Remove sorting.
	    add_action( 'woocommerce_before_shop_loop_item_title', array( $this, 'loop_on_sale_badge' ) );
    }



	/**
	 * Open archive wrapper
	 */
	public function archive_container_open() {
        ?>
        <main id="site-content">
            <div class="container woocommerce-products">
        <?php
    }



	/**
	 * Close archive wrapper
	 */
    public function archive_container_close() {
        ?>
            </div>
        </main>
        <?php
    }



	/**
	 * Display category long description
	 */
	public function category_long_description() {
	    if ( is_product_category() ) :
            $long_description = get_field( 'long_description', get_queried_object() );

		    if ( $long_description ) :
                ?>
                    <article class="category-long-description">
                        <div class="content">
	                        <?php echo $long_description; ?>
                        </div>
                    </article>
                <?php
		    endif;
        endif;
    }



	/**
	 * Product gallery
	 */
	public function product_thumbnail() {
		?>
		<picture>
            <?php echo woocommerce_get_product_thumbnail(); ?>
		</picture>
		<?php
	}



	/**
     * Display all products on shop page
     *
	 * @param object $query
	 * @return object $query
	 */
	public function display_products_shop_page( $query ) {
	    if ( is_shop() ) :
		    $query->set('posts_per_page', -1);
		    $query->set('orderby', 'menu_order');
        endif;

		return $query;
	}



	/**
	 * Adds sale badge
	 */
	public function loop_on_sale_badge() {
		global $product;
		if ( $product->is_on_sale() ) :
			echo '<span class="onsale">' . esc_html__( 'Sale!', 'woocommerce' ) . '</span>';
        endif;
	}


}
