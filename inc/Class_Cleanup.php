<?php
namespace WooBiBoo;

/**
 * Cleanup WorPress unnecessary functions
 *
 * @package woobiboo
 * @author  Mateusz Major
 * @link    https://inspirelabs.pl/
 * @since   2.0
 */
class Class_Cleanup {

	/**
	 * This function provides some actions to improve WP security.
	 */
	public function security() {

		// Clean unecessary actions.
		add_action( 'init', array( $this, 'clean_wp_head' ) );

		// Removes information like version from enqueued files.
		add_filter( 'script_loader_src', array( $this, 'remove_enqueued_files_info' ) );
		add_filter( 'style_loader_src', array( $this, 'remove_enqueued_files_info' ) );

		// Disable WordPress API.
		add_filter( 'rest_authentication_errors', array( $this, 'disable_wp_api' ) );

		// Disable xmlrpc headers.
		add_filter( 'xmlrpc_enabled', array( $this, '__return_false' ) );
		add_filter( 'wp_headers', array( $this, 'remove_xmlrpc_headers' ) );

		// Disable xmlrpc methods.
		add_filter( 'xmlrpc_methods', array( $this, 'remove_xmlrpc_methods' ) );

	}



	/**
	 * This function removes unecessary stylesheets, classes, ids, etc
	 */
	public function clear_style() {

		// Remove customizer custom CSS support.
		add_action( 'customize_register', array( $this, 'remove_customizer_css' ), 15 );

		// Disable emojis.
		add_action( 'init', array( $this, 'disable_emoji' ) );

		// Remove menus classes and ids.
		add_filter( 'page_css_class', array($this, 'cleanup_menus' ), 100, 1 );
		add_filter( 'nav_menu_item_id', array( $this, 'cleanup_menus' ), 100, 1 );
		add_filter( 'nav_menu_css_class', array( $this, 'cleanup_menus' ), 100, 1 );

		// Remove default WP blocks stylesheet.
		add_action( 'wp_enqueue_scripts', array( $this, 'remove_blocks_stylesheet' ) );

	}



	/**
	 * This function removes not so often use comments and ping
	 */
	// public function disable_comments_ping() {

	// 	// Disable comments and trackbacks.
	// 	add_action( 'admin_init', array( $this, 'disable_comments_support' ) );

	// 	// Disable comments and trackbacks on frontend.
	// 	add_filter( 'comments_open', array( $this, 'disable_frontend_comments_support' ), 20, 2 );
	// 	add_filter( 'pings_open', array( $this, 'disable_frontend_comments_support' ), 20, 2 );

	// 	// Hide existing comments.
	// 	add_filter( 'comments_array', array( $this, 'hide_existing_comments' ), 10, 2 );

	// 	// Redirect users whos tryin' access comments on wp-admin.
	// 	add_action( 'admin_init', array( $this, 'disable_admin_comments' ) );

	// 	// Removes comments menu item from wp-admin.
	// 	add_action( 'admin_menu', array( $this, 'remove_comments_menu_item' ) );

	// 	// Removes comments dashboard widget.
	// 	add_action( 'admin_init', array( $this, 'remove_comments_dashboard_widget' ) );

	// 	// Removes comments from admin toolbar.
	// 	add_action( 'admin_bar_menu', array( $this, 'remove_toolbar_items' ), 999 );

	// }



	/**
	 * Additional functions for CF7 plugin.
	 */
	public function contact_form_7() {

		// Removes unecessary tags.
		add_filter( 'wpcf7_form_elements', array( $this, 'clear_cf7' ) );

	}



	/**
	 * Clean unecessary actions.
	 */
	public function clean_wp_head() {
		remove_action( 'wp_head', 'feed_links_extra', 3 );
		remove_action( 'wp_head', 'feed_links', 2 );
		remove_action( 'wp_head', 'rsd_link' );
		remove_action( 'wp_head', 'wlwmanifest_link' );
		remove_action( 'wp_head', 'index_rel_link' );
		remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
		remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
		remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
		remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
		remove_action( 'wp_head', 'wp_generator' );
		remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
		remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
		remove_action( 'xmlrpc_rsd_apis', 'rest_output_rsd' );
		remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
		remove_action( 'template_redirect', 'rest_output_link_header', 11 );
		remove_action( 'wp_head', 'wp_oembed_add_host_js' );
		remove_action( 'welcome_panel', 'wp_welcome_panel' );
	}



	/**
	 * Removes information like version from enqueued files
	 */
	public function remove_enqueued_files_info( $src ) {
		global $wp_version;
		parse_str( wp_parse_url( $src, PHP_URL_QUERY ), $query );
		if ( ! empty( $query['ver'] ) && $query['ver'] === $wp_version ) {
			$src = remove_query_arg( 'ver', $src );
		}
		return $src;
	}



	/**
	 * Disable WordPress API
	 */
	// public function disable_wp_api( $result ) {
	// 	if ( ! empty( $result ) ) {
	// 		return $result;
	// 	}
	// 	if ( ! is_user_logged_in() ) {
	// 		return new \WP_Error( 'rest_not_logged_in', 'You not allowed to view this content.', array( 'status' => 401 ) );
	// 	}
	// 	return $result;
	// }


	/**
	 * Disable xmlrpc headers
	 */
	public function remove_xmlrpc_headers( $headers ) {
		unset( $headers['X-Pingback'] );
		return $headers;
	}



	/**
	 *  Disable xmlrpc methods
	 */
	public function remove_xmlrpc_methods( $methods ) {
		unset( $methods['pingback.ping'] );
		unset( $methods['pingback.extensions.getPingbacks'] );
		return $methods;
	}



	/**
	 * Remove customizer custom CSS support
	 */
	public function remove_customizer_css( $wp_customize ) {
		$wp_customize->remove_section( 'custom_css' );
	}



	/**
	 * Disable emojis for TinyMce
	 */
	public function disable_emoji_tinymce( $plugins ) {
		if ( is_array( $plugins ) ) {
			return array_diff( $plugins, array( 'wpemoji' ) );
		} else {
			return array();
		}
	}



	/**
	 * Disable emojis
	 */
	public function disable_emoji() {
		remove_action( 'admin_print_styles', 'print_emoji_styles' );
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
		remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
		remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
		add_filter( 'tiny_mce_plugins', array( $this, 'disable_emoji_tinymce' ) );
	}



	/**
	 * Remove menus unecessary classes and ids
	 */
	public function cleanup_menus( $var ) {
		$array = is_array( $var ) ? array_intersect( $var, array( 'current-menu-item' )) : '';
		if ( is_array( $array ) && ! empty( $array ) ) {
			array_walk_recursive(
				$array,
				function ( &$value ) {
					if ( 'current-menu-item' === $value ) {
						$value = 'is-active';
					}
				}
			);
		}
		return $array;
	}



	/**
	 * Remove default WP blocks stylesheet
	 */
	public function remove_blocks_stylesheet() {
		wp_dequeue_style( 'wp-block-library' );
		wp_dequeue_style( 'wp-block-library-theme' );
	}



	/**
	 * Disable comments and trackbacks
	 */
	// public function disable_comments_support() {
	// 	$post_types = get_post_types();
	// 	foreach ( $post_types as $post_type ) {
	// 		if( post_type_supports( $post_type, 'comments' ) ) {
	// 			remove_post_type_support( $post_type, 'comments' );
	// 			remove_post_type_support( $post_type, 'trackbacks' );
	// 		}
	// 	}
	// }



	/**
	 * Disable comments and trackbacks on frontend
	 */
	// public function disable_frontend_comments_support() {
	// 	return false;
	// }



	/**
	 * Hide existing comments
	 */
	// public function hide_existing_comments( $comments ) {
	// 	$comments = array();
	// 	return $comments;
	// }



	/**
	 * Redirect users whos tryin' access comments on wp-admin
	 */
	// public function disable_admin_comments() {
	// 	global $pagenow;
	// 	if ( 'edit-comments.php' === $pagenow ) {
	// 		wp_redirect( admin_url() );
	// 		exit;
	// 	}
	// }



	/**
	 * Removes comments menu item from wp-admin
	 */
	// public function remove_comments_menu_item() {
	// 	remove_menu_page( 'edit-comments.php' );
	// }



	/**
	 * Removes comments dashboard widget
	 */
	// public function remove_comments_dashboard_widget() {
	// 	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
	// }



	/**
	 * Removes comments from admin toolbar
	 */
	// public function remove_toolbar_items( $wp_adminbar ) {
	// 	$wp_adminbar->remove_node( 'comments' );
	// }



	/**
	 * Removes unecessary tags
	 */
	public function clear_cf7( $content ) {
		return preg_replace( '/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content );
	}

}

$cleanup = new Class_Cleanup();
$cleanup->security();
$cleanup->clear_style();
// $cleanup->disable_comments_ping();