<?php 
/* 
Template Name: Landing page
*/ 
?>

<?php get_header() ?>




<main id="landingpage-content">
    <?php
if( get_field('enable_fixed_timer') ) { ?>
    <section class="fixed-top-promo">
        <div class="wrapper-fixed">

            <a href="/">
                <img src="<?php the_field('logo_lp_intro') ?>" alt="woobiboo">
            </a>

            <div class="timer_and_btn">
                <?php 
                // sprawdź czy dodano pole
                if( get_field('event_date_time') ):

                // Load field value.
                $date_string = get_field('event_date_time');
                // Create DateTime object from value (formats must match).
                $date = DateTime::createFromFormat('Ymd', $date_string);
                $data_promocji = $date->format('j M Y'); ?>

                <p id="timer" class="timer-area"></p>
                <script>
                // Set the date we're counting down to
                var countDownDate = new Date("<?php echo $data_promocji; ?>").getTime();

                // Update the count down every 1 second
                var x = setInterval(function() {

                    // Get today's date and time
                    var now = new Date().getTime();

                    // Find the distance between now and the count down date
                    var distance = countDownDate - now;

                    // Time calculations for days, hours, minutes and seconds
                    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                    // Output the result in an element with id="demo"
                    document.getElementById("timer").innerHTML = "<span class=lp_pink>" + days + "</span>" +
                        " dni " + "<span class=lp_pink>" + hours + "</span> godzin " + "<span class=lp_pink>" +
                        minutes + "</span> minut " + "<span class=lp_pink>" + seconds + "</span> sekund ";

                    // If the count down is over, write some text 
                    if (distance < 0) {
                        clearInterval(x);
                        document.getElementById("timer").innerHTML =
                            "<?php the_field('informacja_o_zakonczonej_promocji') ?>";
                    }
                }, 1000);
                </script>
                <?php endif; ?>
                <a href="<?php the_field('link_do_promocji_fixed') ?>"
                    class="button button-fixed-timer"><?php the_field('tekst_przycisku_promocji_fixed') ?></a>
            </div>
        </div>
    </section>
    <?php } ?>
    <!-- end fixed timer -->

    <section class="intro-lp">
        <div class="container-intro-lp">
            <div class="intro-wrapper">
                <div class="col-intro">
                    <img src="<?php the_field('logo_lp_intro') ?>" alt="woobiboo">
                    <h1 class="text-title"><?php the_field('tytul_lp_intro') ?></h1>
                    <p><?php the_field('tekst_lp_intro') ?></p>

                    <a class="scroll-anhor" href="#blue-intro">
                        <span class="icon-anhor"></span>
                        <span class="text-anhor"><?php the_field('go_to_bottom_section_lp') ?></span>
                    </a>
                </div>
                <div style="background-image:url(<?php the_field('zdjecie_lp_intro') ?>)" class="col-intro bg-intro">
                </div>
            </div>
        </div>
        <div id="blue-intro" class="blue-area">
            <div class="wrapper-blue-area">
                <h2> <?php the_field('tekst_na_niebieskim_tle') ?></h2>
            </div>
        </div>
    </section>


    <section class="goodbye">
        <div class="wrapper756">
            <div class="icon">
                <img src="<?php the_field('icon_hand_lp') ?>">
            </div>

            <h1><?php the_field('title_section_two_lp') ?></h1>
            <div class="text-area">
                <h3><?php the_field('subtitle_section_two_lp') ?></h3>
                <?php the_field('text_section_two_lp') ?>
            </div>
            <a class="button button_lp"
                href="<?php the_field('button_link_section_two_lp') ?>"><?php the_field('button_text_section_two_lp') ?></a>
            <p class="info_under_button"><?php the_field('tekst_info_under_button_section_two_lp') ?></p>
        </div>
    </section>


    <section class="now4is1">
        <div class="wrapper-content">
            <div class="column-text">
                <div class="icon">
                    <img src="<?php the_field('ikona_section4_lp')?>">
                </div>
                <div class="text-title">
                    <h1><?php the_field('tytul_sekcja_3_lp')?></h1>
                </div>
                <div class="text-content"><?php the_field('tekst_sekcja_3_lp')?></div>

            </div>
            <div class="column-img">
                <?php $image_section_3_lp = get_field('zdjecie_sekcja_3_lp'); ?>
                <img class="rsImg" src="<?php echo $image_section_3_lp['url']; ?>"
                    alt="<?php echo $image_section_3_lp['alt']; ?>" title="<?php echo $image_section_3_lp['title']; ?>">

            </div>
        </div>
    </section>


    <section class="wooHelp">

        <div class="swiper mySwiperWoo">
            <div class="swiper-wrapper">
                <?php 
                if( have_rows('slajder_sekcja_3_lp') ):
                    while( have_rows('slajder_sekcja_3_lp') ) : the_row();
                ?>
                <?php $image_slider_section_3_lp = get_sub_field('slajd_zdjecie_sekcja_3_lp');?>
                <div class="swiper-slide">
                    <img class="sl-item" src="<?php echo $image_slider_section_3_lp['url']; ?>"
                        alt="<?php echo $image_slider_section_3_lp['alt']; ?>"
                        title="<?php echo $image_slider_section_3_lp['title']; ?>">
                </div>

                <?php
endwhile;
else :
endif;
?>
            </div>
        </div>




        <div class="wrapper">
            <div class="icon"><img src="<?php the_field('ikona_section_4_lp')?>" alt=""></div>
            <h1><?php the_field('title_section_4_lp') ?></h1>
            <p><?php the_field('text_section_4_lp') ?></p>
        </div>


        <div class="wrapper-grid-block-white">
            <div class="grid-block-white-lp">
                <?php
            if( have_rows('lista_blokow_woobiboo_pomoze') ):
                while( have_rows('lista_blokow_woobiboo_pomoze') ) : the_row(); ?>
                <div class="item-block-grid">
                    <div class="icon">
                        <img src="<?php the_sub_field('lista_ikona_blok_woobiboo_pomoze') ?>">
                    </div>
                    <h3><?php the_sub_field('tytul_blok_woobiboo_pomoze') ?></h3>
                    <p><?php the_sub_field('tekst_blok_woobiboo_pomoze') ?></p>
                </div>
                <?php     
                endwhile;
                else :
                endif; 
            ?>
            </div>


            <div class="text-under-grid">
                <?php the_field('tekst_pod_blokami_section_4_lp') ?>
            </div>
            <div class="button-area">
                <a class="button section_colours purple"
                    href="<?php the_field('link_przycisku_section_4_lp') ?>"><?php the_field('tekst_przycisku_section_4_lp') ?></a>
                <h6><?php the_field('tekst_pod_przyciskiem_section_4_lp') ?></h6>
            </div>



        </div>

    </section>

    <section class="forYourChild">
        <div class="swiper mySwiperWoo">
            <div class="swiper-wrapper">
                <?php 
                if( have_rows('slajder_sekcja_5_lp') ):
                    while( have_rows('slajder_sekcja_5_lp') ) : the_row();
                ?>
                <?php $image_slider_section_5_lp = get_sub_field('slajd_zdjecie_sekcja_5_lp');?>
                <div class="swiper-slide">
                    <img class="sl-item" src="<?php echo $image_slider_section_5_lp['url']; ?>"
                        alt="<?php echo $image_slider_section_5_lp['alt']; ?>"
                        title="<?php echo $image_slider_section_5_lp['title']; ?>">
                </div>

                <?php
endwhile;
else :
endif;
?>
            </div>
        </div>
        <div class="wrapper-area">

            <div class="wrap-grid">
                <div class="col-text-left">
                    <div class="icon"><img src="<?php the_field('ikona_section_5') ?>"></div>
                    <h1 class="text-title"><?php the_field('title_section_5') ?></h1>
                    <?php the_field('text_section_5') ?>
                </div>
                <div style="background-image:url(<?php the_field('bg_cover_right_col_section_5') ?>)"
                    class="col-bg-right">
                </div>
            </div>
        </div>
    </section>




    <section class="wooHelp no_compromise">


        <div class="wrapper">
            <div class="icon"><img src="<?php the_field('ikona_section_6_lp')?>" alt=""></div>
            <h1><?php the_field('title_section_6_lp') ?></h1>
            <?php the_field('text_section_6_lp') ?>
        </div>


        <div class="wrapper-grid-block-white">
            <div class="grid-block-white-lp">
                <?php
            if( have_rows('lista_blokow_woobiboo_section_6') ):
                while( have_rows('lista_blokow_woobiboo_section_6') ) : the_row(); ?>
                <div class="item-block-grid">
                    <div class="icon">
                        <img src="<?php the_sub_field('lista_ikona_blok_woobiboo_section_6') ?>">
                    </div>
                    <?php if( get_sub_field('tytul_blok_woobiboo_section_6') ): ?>
                    <h3><?php the_sub_field('tytul_blok_woobiboo_section_6') ?></h3>
                    <?php endif; ?>
                    <p><?php the_sub_field('tekst_blok_woobiboo_section_6') ?></p>
                </div>
                <?php     
                endwhile;
                else :
                endif; 
            ?>
            </div>

            <?php if( get_field('tekst_pod_blokami_section_6') ): ?>
            <div class="text-under-grid">
                <?php the_field('tekst_pod_blokami_section_6') ?>
            </div>
            <?php endif; ?>
            <div class="button-area">
                <a class="button section_colours"
                    href="<?php the_field('link_przycisku_section_6') ?>"><?php the_field('tekst_przycisku_section_6') ?></a>
                <h6><?php the_field('tekst_pod_przyciskiem_section_6') ?></h6>
            </div>
        </div>
    </section>

    <section class="goodbye low">
        <div class="wrapper756">
            <div class="icon">
                <img src="<?php the_field('icon_hand_lp_section_7') ?>">
            </div>

            <h1><?php the_field('title_section_two_lp_section_7') ?></h1>
            <div class="text-area">
                <?php if( get_field('subtitle_section_two_lp_section_7') ): ?>
                <h3><?php the_field('subtitle_section_two_lp_section_7') ?></h3>
                <?php endif; ?>
                <?php the_field('text_section_two_lp_section_7') ?>
            </div>

        </div>
    </section>


    <section class="wooHelp wooGuarantee">

        <div class="swiper mySwiperWoo">
            <div class="swiper-wrapper">
                <?php 
                if( have_rows('slajder_sekcja_8_lp_kopia') ):
                    while( have_rows('slajder_sekcja_8_lp_kopia') ) : the_row();
                ?>
                <?php $image_slider_section_8_lp = get_sub_field('slajd_zdjecie_sekcja_8_lp');?>
                <div class="swiper-slide">
                    <img class="sl-item" src="<?php echo $image_slider_section_8_lp['url']; ?>"
                        alt="<?php echo $image_slider_section_8_lp['alt']; ?>"
                        title="<?php echo $image_slider_section_8_lp['title']; ?>">
                </div>

                <?php
endwhile;
else :
endif;
?>
            </div>
        </div>


        <div class="wrapper">
            <div class="icon"><img src="<?php the_field('icon_hand_lp_section_8')?>" alt=""></div>
            <h1><?php the_field('title_section_two_lp_section_8') ?></h1>
            <p><?php the_field('subtitle_section_two_lp_section_8') ?></p>
            <?php the_field('text_section_two_lp_section_8') ?>
        </div>


        <div class="wrapper-grid-block-white">
            <div class="grid-block-white-lp">
                <?php
            if( have_rows('lista_blokow_woobiboo_section_8') ):
                while( have_rows('lista_blokow_woobiboo_section_8') ) : the_row(); ?>
                <div class="item-block-grid grid-transparent lp_block_float_left">
                    <div class="icon">
                        <img src="<?php the_sub_field('lista_ikona_blok_woobiboo_section_8') ?>">
                    </div>

                    <div class="lp_block_float_right">
                        <?php if( get_sub_field('tytul_blok_woobiboo_section_8') ): ?>
                        <h3><?php the_sub_field('tytul_blok_woobiboo_section_8') ?></h3>
                        <?php endif; ?>
                        <p><?php the_sub_field('tekst_blok_woobiboo_section_8') ?></p>

                    </div>

                </div>
                <?php     
                endwhile;
                else :
                endif; 
            ?>
            </div>

            <a name="toOrder"></a>
            <?php if( get_field('tekst_pod_blokami_section_8') ): ?>
            <div class="text-under-grid">
                <?php the_field('tekst_pod_blokami_section_8') ?>
            </div>

            <?php endif; ?>
        </div>

        <div class="LP_display_category">

            <?php 
		
			$LP_button_buy_text = get_field('LP_tekst_przycisku_KUP_TERAZ');
			$LP_button_preorder_text = get_field('LP_tekst_przycisku_przedsprzedaz');
			$LP_category_id = get_field('lp_8_wybierz_kategorie_produktow_do_wczytania');
		
		
		?>

            <ul class="products">
                <?php
				$args = array( 'post_type' => 'product', 'posts_per_page' => 3, 'product_cat' => $LP_category_id, 'orderby' => 'rand' );
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>

                <li class="product">

                    <a href="<?php echo get_permalink( $loop->post->ID ) ?>"
                        title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>">

                        <div class="tag_preorder"><?php echo $LP_button_preorder_text; ?></div>


                        <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="300px" height="300px" />'; ?>

                        <h3><?php the_title(); ?></h3>
                        <span class="price"><?php echo $product->get_price_html(); ?></span>
                        <button class="LP_buy_now"><?php echo $LP_button_buy_text; ?></button>

                    </a>


                </li>

                <?php endwhile; ?>
                <?php wp_reset_query(); ?>

            </ul>
        </div>


    </section>







</main>







<?php get_footer() ?>