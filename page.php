<?php get_header(); ?>

<main id="site-content">
    <?php
        do_action('fh-page-before-content');
        if ( have_posts() ) :
            while ( have_posts() ) : the_post();
            ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class('container' ); ?>>

        <?php do_action( 'fh-page-content' ) ?>

    </article>
    <?php
            endwhile;
        endif;
        do_action('fh-page-after-content');
        ?>
</main>

<?php get_footer(); ?>