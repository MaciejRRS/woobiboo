<?php
/*
 Template Name: Blog
 */
?>
<?php
get_header(); ?>
<main id="blog-rrs" role="main">


    <section class="title-area">
        <div class="container">

            <nav class="breadcrumb d-flex align-items-center" aria-label="breadcrumb">
                <?php
        if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
        }
    ?>
            </nav>


            <h1><?php the_title(); ?></h1>
            <div class="short-text">
                <?php the_content(); ?>
            </div>
        </div>
    </section>



    <?php
// Protect against arbitrary paged values
$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;


 
$args = array(
    'post_type' => 'post',
    'post_status'=>'publish',
    'paged' => $paged,
);

$the_query = new WP_Query($args);
?>

    <?php if ( $the_query->have_posts() ) : ?>


    <section class="news">
        <div class="container">
            <div class="blocks-blog">
                <div class="row-wrap">

                    <!-- the loop -->
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                    <div class="column-4">
                        <a href="<?php echo get_permalink(); ?>">
                            <div class="news-item">
                                <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                                             echo '<div style="background: url('. $url.');" class="bg-post-img">'; ?>
                            </div>

                            <h2 class="news-title-post"><?php echo get_the_title(); ?>
                            </h2>
                            <div class="excerptNewsArea">
                                <?php echo the_excerpt(); ?>
                            </div>

                            <p class="read_more"><?php the_field('przycisk_czytaj_wiecej','options') ?> >></p>
                    </div>
                    </a>
                </div>




                <?php endwhile; ?>
                <!-- end of the loop -->

                <?php
                wp_reset_query();
                ?>




            </div>
        </div>
        <div class="pagination">
            <?php
								echo paginate_links( array(
									'format'  => 'page/%#%',
									'current' => $paged,
									'total'   => $the_query->max_num_pages,
									'mid_size'        => 2,
									'prev_text'       => __('&laquo;'),
									'next_text'       => __('&raquo;')
								) );
							?>
        </div>
        <?php endif; ?>
        </div>
    </section>
</main><!-- .site-main -->
<?php get_footer(); ?>