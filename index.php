<?php get_header(); ?>

<?php
if ( have_posts() ) :
	while ( have_posts() ) : the_post();
		?>
        <article id="post-<?php the_ID(); ?>" <?php post_class('container' ); ?>>

			<?php the_title('<h1>','</h1>'); ?>
			<?php the_content(); ?>

        </article>
	<?php
	endwhile;
endif;
?>

<?php get_footer(); ?>