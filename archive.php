<?php
/**
 * Defeault archive template
 *
 * @package amber-body
 * @since 1.0.0
 */

get_header();
?>

<main id="main" class="site-main" role="main">

	<?php do_action( 'amberbody_loop_before' ); ?>

	<?php
	if ( have_posts() ) :
		?>
		<div class="am-loop">
			<?php
			while ( have_posts() ) :

				the_post();

			endwhile;
			?>
		</div>
		<?php
	endif;
	?>

	<?php
	do_action( 'amberbody_loop_after' );
	?>

</main>

<?php
get_footer();
