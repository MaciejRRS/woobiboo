'use strict';

/*
 * Variables
 * Change this settings to start
 */

// Localhost page url address
var url_address = 'http://woobiboo.test/';

// Requirements
var gulp = require('gulp');

// SCSS
var sass = require('gulp-sass');
sass.compiler = require('node-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');

// JavaScripts
var terser = require('gulp-terser');

// Utilities
var browserSync = require('browser-sync').create();


/*
 * SCSS Compiler Development
 */
gulp.task('sass-development', function () {
	return gulp.src('./assets/scss/**/*.scss')
		.pipe(sourcemaps.init({
			largeFile: true
		}))
		.pipe(
			sass({
				errorLogToConsole: true
			})
				.on('error', console.error.bind(console))
		)
		.pipe(
			sourcemaps.write('./')
		)
		.pipe(
			gulp.dest('./assets/css')
		);
});


/*
 * Gulp wathch
 */
gulp.task('watch', function () {
	browserSync.init({
		proxy: url_address
	});
	gulp.watch('./assets/scss/**/*.scss', gulp.task('sass-development'));
	gulp.watch('./assets/js/**/*.js', gulp.task('js'));
	gulp.watch('./**/*.{html,php}', browserSync.reload);
});