<?php get_header() ?>


<main id="blog-rrs" role="main">


    <section class="title-area">
        <div class="container">

            <nav class="breadcrumb d-flex align-items-center" aria-label="breadcrumb">
                <?php
        if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
        }
    ?>
            </nav>
            <div class="single-post-title">
                <h1><?php the_title() ?></h1>
            </div>
            <div class="content-post-wrap">
                <?php the_content();?>
            </div>
        </div>

    </section>
</main>


<?php get_footer(); ?>